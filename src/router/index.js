import Vue from 'vue'
import VueRouter from 'vue-router'

import routes from './routes'

Vue.use(VueRouter)

/*
 * If not building with SSR mode, you can
 * directly export the Router instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Router instance.
 */

export default function ( store /* { ssrContext } */) {
  const router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,

    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })

  // process the following before access this route
  router.beforeEach((to, from, next) => {

    /*
    
{
      path: '/login'
    }    
    next({
      path: from.fullPath
    });*/


    // page(s) requested for login
    if (to.matched.some(record => record.meta.requiresAuth)) {
    
      // chk login status
      //=========================
      //console.log("protected page ");
      //console.log(this.$store.state.user.username);
      //console.log("protected page 1 ", loginLib);

      //if (localStorage.getItem('jwt') == null) {
      // call Login Module global loginFlag
      if (loginLib.goLogin() == false) {

        // in case not yet login
        // todo: a err. as "Uncaught (in promise) undefined", not yet know reason

        //console.log("to ", to);
        loginLib.preRoute = to.fullPath;
        //console.log("from ", from);
    
        //next(
          //{
          //  path: to.fullPath
          //}    
        //)
        //  next({
        //  path: from.fullPath
        //});
          //,
          //params: { nextUrl: to.fullPath }

      } else {
        // in case already login
        next()
        //let user = JSON.parse(localStorage.getItem('user'))
        //if(to.matched.some(record => record.meta.is_admin)) {
        //    if(user.is_admin == 1){
        //        next()
        //    }
        //    else{
        //        next({ name: 'userboard'})
        //    }
        //}else {
        //    next()
        //}
      }
        
    // page(s) for public
    } else if(to.matched.some(record => record.meta.guest)) {
      console.log("public page");
      next();

    // Final for safty
    } else {
      console.log("finally");
      next()
    }
    
  })

  return router
}

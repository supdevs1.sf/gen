
const routes = [

  // Main page
  {
    path: '/',
    name: 'main',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/Index.vue') }
    ]
  },

  // Login Page
  /*{
    path: '/login',
    name: 'login',
    component: () => import('pages/Login.vue'),
    meta: { 
        guest: true
    }
  },*/

  // Dev. Env.
  {
    path: '/devenv',
    name: 'devenv',
    component: () => import('pages/DevEnv.vue'),
    children: [
      { path: '', component: () => import('pages/DevEnv1.vue') }
    ],
    meta: { 
        requiresAuth: true
    }
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes

/*
 * This source file keeps some dialog(s) for sharing to different display components. 
 * This Code reference fr.:
 *  - https://quasar.dev/quasar-plugins/dialog
 *  - https://stackoverflow.com/questions/58557479/reuse-quasar-dialog-plugin-with-custom-component-on-another-component
 */

/*
 * =====================================================
 * Common Dialog Code 
 */
import { Dialog } from 'quasar'

/*
 * =====================================================
 * Main Msg. Dialog
 */
export function newMsgDiag(UsingThisDiagComponent, msg) {

    return Dialog.create({
        component:UsingThisDiagComponent,
        msg: msg,
    }).onOk(() => {
        console.log('OK')
    }).onCancel(() => {
        console.log('Cancel')
    }).onDismiss(() => {
        console.log('I am triggered on both OK and Cancel')
    })
}
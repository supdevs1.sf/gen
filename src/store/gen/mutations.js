/*
export function someMutation (state) {
}
*/

// Login Flag
export const updateLoginFlag = (state, opened) => {
    state.loginFlag = opened
}  

// login user info.
export const updateUserInfo = (state, opened) => {
    state.userInfo = opened
}

// add a project into projects array
export const addProj = (state, proj) => {
    state.projs.push({'projName': proj});
}

// remove a project fr. projects array
export const removeProj = (state, proj) => {

    // get index of object with id:37
    //console.log('indexof ', state.projs.indexOf({'projName': proj}));
    var removeIndex = state.projs.map(function(item) { return item.projName; }).indexOf(proj);

    // remove object
    state.projs.splice(removeIndex, 1);

}

// clr projects array
export const clrProjs = (state) => {
    state.projs = [];
}

// selected project info.
export const updateProj = (state, opened) => {
    state.proj = opened
}

// selProjState Flag
export const updateSelProjState = (state, opened) => {
    state.selProjState = opened
}  

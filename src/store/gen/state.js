// Always use a function to return state if you use SSR
export default function () {
  return {
    loginFlag: 0
    , userInfo: {}
    , projs: []

    , proj: {}  // selected proj.
                // {
                //  'name':projName
                //  , "soln": that.solution
                //  , "sshport": ret.sshport
                //  , "webport": ret.webport
                // }

    , selProjState: false
  }
}
// import something here

import axios from 'axios'

// Example: this.$axios will reference Axios now so you don't need stuff like vue-axios

// "async" is optional;
// more info on params: https://quasar.dev/quasar-cli/cli-documentation/boot-files#Anatomy-of-a-boot-file
export default async ({ Vue /* app, router,  ... */ }) => {
  // something to do

  // we add it to Vue prototype
  // so we can reference it in Vue files
  // without the need to import axios
  Vue.prototype.$axios = axios
}

# Generator (gen)

Generator for auto. system creation

## Technical Operation

### Start the app in development mode (hot-code reloading, error reporting, etc.)
```bash
quasar dev
```


### Build the app for production
```bash
quasar build
```

### Customize the configuration
See [Configuring quasar.conf.js](https://quasar.dev/quasar-cli/quasar-conf-js).


## Configuration

### Design

- Blk Diag.

  - Whole User Application and Dev. Env. relationship
    ```
                                                    +------------------------------------------------------+
                                                    |  User App. Frontend                                  |
                                                    |                                                      |
                                                    |   +------------------------------------------------+ |
                                                    |   | Dev. Env. Backend                              | |
                                                    |   | +---------------+                              | |
                                                    |   | |               |                              | |
                                                    |   | | Frappe        |                              | |
                       +------------------------+   |   | |               |                              | |
                       |  Dev. Env. Frontend    |   |   | +---+-----------+                              | |
                       |                        +------>+     |                                          | |
                       |                        |   |   |     |                                          | |
                       +-------------------+----+   |   |     |                      +-----------------+ | |
                                           |        |   |     |                      |    +----------+ | | |
                                           |        |   |     |            +------------->+container | | | |
    +--------------------------------+     |        |   |     |            |         |    |#1        | | | |
    |  User App. Backend             |     |        |   |     |            |         |    +----------+ | | |
    |   +------------------------+   |     |        |   |     |            |         |                 | | |
    |   |  Dev. Env. Backend     |   |     |        |   |     |            |         |    +----------+ | | |
    |   |                        |   +<----+        |   |     |            |    +-------->+container | | | |
    |   |                        |   |              |   |     v            |    |    |    |#2        | | | |
    |   +------------------------+   |              |   | +---+------------++   |    |    +----------+ | | |
    |                                |              |   | | create          +---+    |                 | | |
    +--------------------------------+              |   | | new container   |        |    +----------+ | | |
                                                    |   | |                 +------------>+container | | | |
                                                    |   | | by              |        |    |#3        | | | |
                                                    |   | | docker+compose  |        |    +----------+ | | |
                                                    |   | +-----------------+        | Containers      | | |
                                                    |   |                            +-+-+-------------+ | |
                                                    |   |                              ^ ^               | |
                                                    |   |                              | |               | |
                                                    |   +------------------------------------------------+ |
                                                    +------------------------------------------------------+
                                                                                      | |
                        +----------------------------------------------------+        | |
                        |  Dev. Env. Repos (containing libs / modules)       |        | |
                        |  by docker containers                              +--------+ |
                        |                                                    +----------+
                        |                                                    |
                        +----------------------------------------------------+
    ```

  - User Application Frontend, can be diff. server fr. User Application Backend
    ```
                                       +------------------+
                                       |Get Fr. Generator |
                                       +-------------+----+
                                                     |
    +---------------------------------------------------------------------------------+
    |    Any Linux Server(s) (including Virtualbox)  |                                |
    |    Your Dev. Env. both Frontend & Backend      v                                |     +-----------------------+
    |                                  +-------------+-----------------------------+  |     |  User Desktop         |
    |     +--------------+             |  Dev. Env.                                |  |     |                       |
    |     | Login Module |             |                     +------------+        |  |     |   +---------+         |
    |     |              |             |            +------->+ Web Server +<--------------------+ Browser |         |
    |     | +----------+ |             |            |        +------------+        |  |     |   +-+-------+         |
    |     | | Register | |             |            |                              |  |     |     |                 |
    |     | | / Login  | |   +-------->+  +---------+-+      +------------+        |  |     |     |   +-----+       |
    |     | |          | |   |         |  |           |  +-->+ Online IDE +<----------------------+   | IDE |       |
    |     | |  Frappe  | +---+         |  |           +--+   +------------+        |  |     |         +--+--+       |
    |     | +---+------+ |   |User ID  |  | Workspace |                            |  |     |            |          |
    |     |     |        |   |         |  |           +--+                         |  |     |            v          |
    |     |     v        |   |         |  +------+----+  |   +-------------+       |  |     |   +--------+----+     |
    |     |   +-+--+     |   |         |         |       +-->+ SFtp Server +<-------------------+ SFtp Client |     |
    |     |   |User|     |   |         |         |           +-------------+       |  |     |   +-------------+     |
    |     |   |Db  |     |   |         |         |                                 |  |     |                       |
    |     |   +----+     |   |         |         |        +---------------------+  |  |     |    +----------+       |
    |     |              |   |         |         +------->+ Tool Chain Terminal +<---------------+ Terminal |       |
    |     |              |   |         |                  +---------------------+  |  |     |    +----------+       |
    |     +--------------+   |         |                                           |  |     |                       |
    |                        |         +-------------------------------------------+  |     +-----------------------+
    |                        |                 User App Frontend                      |
    |                        |                                                        |
    +---------------------------------------------------------------------------------+
                             |
                             v User App Backend

    TODO: git hwoto
    ```
    - Workspace -- keeps at inside frappe site.

  - User Application Backend, can be diff. server fr. User Application Frontend
    ```
                                     +------------------+
              User ID                |Get Fr. Generator |
                 +                   +---------+--------+
                 |                             |
                 |                             |
                 v                             v
    +------------+-----------------------------+--------------------------+    +-----------------------+
    |                          Any Linux Server (including Virtualbox)    |    |  User Desktop         |
    |                          Your Dev. Env. both Frontend & Backend     |    |                       |
    |                                               +------------+        |    |   +---------+         |
    |           +-----------------------------------+ Web Server +<----------------+ Browser |         |
    |           |                                   +------------+        |    |   +-+-------+         |
    |           |          +-----------------+                            |    |     |                 |
    |           |          |   Workspace     |      +------------+        |    |     |   +-----+       |
    |           |          |                 |  +-->+ Online IDE +<------------------+   | IDE |       |
    |           |          | +-------------+ +--+   +------------+        |    |         +--+--+       |
    |           |          | |Frappe App #1| |                            |    |            |          |
    |           v          | +-------------+ +--+                         |    |            v          |
    |         +-+------+   |                 |  |   +-------------+       |    |   +--------+----+     |
    |         |        |   | +-------------+ |  +-->+ SFtp Server +<---------------+ SFtp Client |     |
    |         | Frappe +-->+ |Frappe App #2| |      +-------------+       |    |   +-------------+     |
    |         |        |   | +-------------+ |                            |    |                       |
    |         +--------+   |                 |   +---------------------+  |    |    +----------+       |
    |                      | +-------------+ +-->+ Tool Chain Terminal +<-----------+ Terminal |       |
    |                      | |Frappe App #3| |   +---------------------+  |    |    +----------+       |
    |                      | +-------------+ |                            |    |                       |
    |                      |                 |                            |    +-----------------------+
    |                      +-----------------+                            |
    |                                                                     |
    |      User App Backend Dev. Env.                                     |
    +---------------------------------------------------------------------+
    ```

  - This is a Dev. Env. in both Dev. Env. Frontend & Dev. Env. Backend which can be in one / two Linux Server(s).
    ```
    
    ```

  - "Generator Production" and "Generator Development" relationship as a example.
    ```
    +---------------------+
    |                     |
    | Generator Frontend  |
    | (Production)        |
    |                     |
    | Nginx Web Server    |
    |                     |
    +--------+------------+
             |                +--------------------------------+
             |                | Generator Frontend             |
        +----+----------------+ (Development)                  +-----------+                 +---------------------+
        |                     |                                |           v                 |                     |
        |                     |  As one of Development Project |      +----+-----------------+    Dev Sub-App      |
        |                     |                                |      |                      |                     |
        |                     +-----------------+--------------+      | Generator Backend    +-----+------------+--+
        | Generator Backend                     |                     | (Development)              ^            |
        | (Production)                          |                     |                            |            |
        |                                       |                     |                            |            |
        |                                       |                     | Frappe - Gnl App +-+-------+            |
        | Frappe - Gnl App +-+-+ Dev Sub-App    |                     | (Development)      |                    |
        | (Production)       |                  |                     |                    +-+ Login Sub-App    |
        |                    +-+ Login Sub-App  |                     |                                         |
        |                                       |                     |                                         |
        +---------------------------------------+                     +-----------------------------------------+
    ```
